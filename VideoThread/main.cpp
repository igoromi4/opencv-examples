#include <iostream>
#include <pthread.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


class VideoThread
{
public:
    cv::Mat frame;
    cv::Mat newFrame;
    pthread_mutex_t waitFrame;
    pthread_mutex_t waitRead;
    pthread_t thread;
    VideoCapture &capture;

    VideoThread(VideoCapture &cap);
//    CameraThread(int id);
//    CameraThread(string filename);
    ~VideoThread();

    bool readFrame();
    static void* readCamera(void *context);
    VideoThread& operator>>(Mat &frame);

private:
    void init();
};

void VideoThread::init()
{
    int code;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
//    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);

    code = pthread_mutex_init(&waitFrame, &attr);

    if (code != 0) {
        cerr << "pthread_mutex_init() exit code: " << code << endl;
    }

    pthread_mutex_lock(&waitFrame);

    code = pthread_mutex_init(&waitRead, &attr);

    if (code != 0) {
        cerr << "pthread_mutex_init() exit code: " << code << endl;
    }

    pthread_create(&thread, NULL, VideoThread::readCamera, (void*)this);
    pthread_detach(thread);
}

VideoThread::VideoThread(VideoCapture &cap) :
    capture(cap)
{
    readFrame();

    init();
}

VideoThread::~VideoThread()
{
    int code = pthread_mutex_destroy(&waitFrame);

    if (code != 0) {
        cerr << "pthread_mutex_destroy() exit code: " << code << endl;
    }
}

bool VideoThread::readFrame()
{
    capture >> newFrame;

    if (newFrame.empty()) {
        cerr << "Capture closed" << endl;
        return false;
    }

    cv::swap(frame, newFrame);

    return true;
}

void* VideoThread::readCamera(void *context)
{
    VideoThread* self = (VideoThread*)context;

    while (1) {
        pthread_mutex_lock(&(self->waitRead));

        if (!self->readFrame()) {
            break;
        }

        pthread_mutex_unlock(&(self->waitFrame));
    }
}

VideoThread& VideoThread::operator>>(Mat &frame)
{
    pthread_mutex_lock(&waitFrame);
    frame = this->frame;
    pthread_mutex_unlock(&waitRead);
//    this->frame.copyTo(frame);

    return *this;
}


int main() {
    int64 start_tick, end_tick;
    double frame_time = 0;
    double fps = 0;

    int64 read_start_tick, read_end_tick;
    double read_time = 0;

    int taskTime = 5;

    bool useCapture = false;

    VideoCapture capture(0);
    capture.set(CAP_PROP_FPS, 30);
    VideoThread video(capture);
    Mat frame;

    while (1) {
        start_tick = cv::getTickCount();

        read_start_tick = cv::getTickCount();
        if (useCapture) {
            capture >> frame;
        } else {
            video >> frame;
        }
        read_end_tick = cv::getTickCount();
        read_time = (double)(read_end_tick - read_start_tick) / cv::getTickFrequency();

        putText(frame, "FPS: " + to_string(fps), Point(10, 15), 1, 1, Scalar(50, 50, 50));
        putText(frame, "Read time: " + to_string(read_time), Point(10, 30), 1, 1, Scalar(50, 50, 50));

        imshow("frame", frame);

        switch (waitKey(taskTime)) {
            case 'q':
                return 0;
            case 's':
                useCapture = !useCapture;
                cout << "useCapture = " << useCapture << endl;
        }

        end_tick = cv::getTickCount();
        frame_time = (double)(end_tick - start_tick) / cv::getTickFrequency();
        fps = 1.0d / frame_time;
    }

    return 0;
}
